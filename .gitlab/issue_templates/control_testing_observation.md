## Observation Description

<!-- Add a title to this issue that will communicate the observation clearly -->

<!-- Replace this section with an explanation of the observation -->

## Observation Owner

* @ <!-- Replace with GitLab username
In most cases the observation owner is the control owner since this is normally the person with the authority and responsibility to define the processes associated with the security control related to this observation -->

## Remediation Recommendations

<!-- The security compliance team is often best placed to understand what exactly needs to change with documentation or process in order for a testing observation to be remediated. Since you have tested the process and understand what isn't designed or operating correctly, you should identify what boring solution could be used to quickly remediate the gap you have identified. Keep the following things in mind as you make these recommendations:
1. These recommendations should be actionable so don't just state what's not working, make sure you're identifying what additional process could be put in place that would remediate this observation.
2. Don't forget to incorporate iteration where possible. We don't need this control to immediate jump to a state of maturity that surpasses our immediate audit requirements. When possible you should structure this iteration in the form of phases as in the following example:
   * Phase 1: Offboarding issues should not be closed until all steps have been verified and a confirmation comment has been added by the off-boarding DRI.
   * Phase 2: Automation should be created that checks for off-boarding issues without all tasks completed and alerts the off-boarding DRI that some requirements haven't been completed.
   * Etc.  -->

## Remediation Plan

<!-- This Remediation Plan can take 2 forms:
1. A link to a separate issue where the remediation work has been defined and is being tracked
2. A list of high-level milestones and the associated due dates of those milestones listed here -->

## Remediation Due Date

<!-- Add the due date for the remediation here and also assign this due date to this issue. This due date is assigned to by the security compliance team and represents when we need remediation to be completed by in order to meet our compliance and regulatory requirements. It's entirely possible this will be negotiated and might have to be escalated before the observation owner and security compliance analyst agree, but this due date should be set when the issue is created in order to kick off that process as soon as possible. -->

## Observation Notes

<!-- Any additional notes about the observation should be captured here. To include: additional details, hurdles to overcome, circumstances leading to observation or that make the observation unique etc. -->

## Remediated Date

<!-- This is the actual remediation date for the observation. This should be completed after remediation is confirmed. If a control is determined to be remediated as of a date, this would be the spot to put that date. -->

## Security Compliance Analyst Action Items

- [ ] Assign the appropriate risk rating label
<!-- Refer to the [Observation Management handbook page](https://about.gitlab.com/handbook/engineering/security/security-assurance/security-compliance/observation-management.html) for guidance on how to assign a risk-rating for this observation.
   * ~"RiskRating::OFI"
   * ~"RiskRating::Low"
   * ~"RiskRating::Moderate"
   * ~"RiskRating::High"
   -->
- [ ] Assign the observation status label
<!-- 
   * ~"ObservationStatus::Identified"
   * ~"ObservationStatus::Assigned"
   * ~"ObservationStatus::Remediation in progress"
   * ~""ObservationStatus::Resolved"
   * ~"ObservationStatus::Ignored/Invalid" 
   See /README.md for an explanation of each status
    -->
- [ ] Add the appropriate system scope label
<!-- Add the appropriate system scope label to this issue. If the system you are testing does not have a label, create a label for your system using the naming convention `SystemScope::SystemName` -->

/label ~"source::ControlTest"

/confidential

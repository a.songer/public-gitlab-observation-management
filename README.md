# Observation Management

This repo contains the open-source version of GitLab's Observation Management Program. This project can be used to manage observations (a.k.a. Tier 3 information security risks) from creation, to reporting, and through to closure.

## Context, overview, and methodology

The [GitLab Observation Management Handbook Page](https://about.gitlab.com/handbook/engineering/security/security-assurance/security-compliance/observation-management.html) has detailed information on how our observation management program is structured and the methology we use to manage these observations. That handbook page should be the starting point in understanding this program as a whole.

## How to use this project

If you're interested in using the GitLab Observation Management Program to manage observations for your organization, you should [follow these instructions](https://gitlab.com/gitlab-com/gl-security/public-gcf/edit) for exporting this project and the importing the project within your organization's repo.

## Observation phases explained

Observations move through various states as they go through the program. These states are used to identify where eacah observation is in its lifecycle and also to gather observations together in a meaningful way for the purposes of reporting.

1. Identified: This is the state each observation starts in once it is created. It is the phase where the observation issue has been created but has not yet been validated with the remediation owner.
1. Assigned: This is the state of the observation once you have reached out to the remediation owner and asked them to confirm the validity of the observation.
1. Remediation in Progress: This is the state of the observation when the finding has been validated and the remediation owner has identified a remediation plan.
1. Ignored/Invalid: This is the state of an observation that either has no value in remediating (perhaps due to a change in the underlying compliance requirement such that the requirement driving the change no longer exists) or if the remediation owner has confirmed that the observation itself was invalid (for example if you created the observation based on a misunderstanding of the technical capabilities or limitations of the system being tested)
1. Resolved: Remediation has been completed by remdiation owner and the security compliance team can retest the original control to validate the remediation's efficacy.

## Creating a new observation issue:

[Issue templates](https://docs.gitlab.com/ee/user/project/description_templates.html) are useful for this project because you can create a new template for each source of observations. For example, you can have an issue template for control testing observations, another for vendor security review observations, another for risk triage observations, etc.

The following link will take you to a new issue using the related issue template. Detailed instructions for how to fill out each issue template are contained as comments within the template itself. Please make sure to follow all guidance within each issue template to ensure consistency across observation issues.

* [Click here to create a new Control Testing observation issue](https://gitlab.com/gitlab-com/gl-security/public-gitlab-observation-management/-/issues/new?issuable_template=control_testing_observation)

## What to do if you don't see an issue template for an observation you're creating

If you don't see a link above for the type of observation for which you need to create an observation, that's a great way to identify the new for a [new observation issue template](https://docs.gitlab.com/ee/user/project/description_templates.html)

## View existing observation issues

* [Observations grouped by the source of how they were identified](https://gitlab.com/gitlab-com/gl-security/public-gitlab-observation-management/-/boards/2636012)
* [Observations grouped by the Risk Rating of the observation](https://gitlab.com/gitlab-com/gl-security/public-gitlab-observation-management/-/boards/2636013)
* [Observations grouped by the current status of the observation](https://gitlab.com/gitlab-com/gl-security/public-gitlab-observation-management/-/boards/2636015)

## Resources

* More about the observation management program can be found in [this handbook page](https://about.gitlab.com/handbook/engineering/security/security-assurance/security-compliance/observation-management.html)
* Observations and Testing are parts of the overall Security Control Lifecycle. An overview of that lifecycle with additional supporting information can be found in the [SCLC handbook page](https://about.gitlab.com/handbook/engineering/security/security-assurance/security-compliance/security-control-lifecycle.html)
